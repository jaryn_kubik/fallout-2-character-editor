﻿using System;

namespace FalloutCE
{
    public class Traits
    {
        public int Trait1 { get; set; }
        public int Trait2 { get; set; }
        public string[] Names { get; private set; }

        public Traits(IntPtr proc)
        {
            Names = new string[16];
            for (int i = 0; i < 16; i++)
                Names[i] = Memory.ReadString(proc, Memory.Traits + i * 0x0C);

            Trait1 = Memory.ReadInt32(proc, Memory.TraitValues);
            Trait2 = Memory.ReadInt32(proc, Memory.TraitValues + 4);
        }

        public void Save(IntPtr proc)
        {
            Memory.WriteInt32(proc, Memory.TraitValues, Trait1);
            Memory.WriteInt32(proc, Memory.TraitValues + 4, Trait2);
        }
    }
}