﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FalloutCE
{
    public class Perks
    {
        public IEnumerable<PerkInfo> Values { get { return Data.Where(p => p.Value != 0); } }
        public PerkInfo[] Data { get; private set; }
        public byte Remaining { get; set; }

        private static readonly Perks instance = new Perks();
        public static Perks Instance { get { return instance; } }

        public void Reload(IntPtr proc)
        {
            Data = new PerkInfo[119];
            IntPtr structure = (IntPtr)Memory.ReadInt32(proc, Memory.PerksPointer);
            IntPtr values = (IntPtr)Memory.ReadInt32(proc, Memory.PerksValuesPointer);
            for (int i = 0; i < 119; i++)
            {
                Data[i] = new PerkInfo
                {
                    Name = Memory.ReadString(proc, structure + i * 0x4C),
                    Ranks = Memory.ReadInt32(proc, structure + i * 0x4C + 0x0C),
                    Stat = Memory.ReadInt32(proc, structure + i * 0x4C + 0x14),
                    StatMod = Memory.ReadInt32(proc, structure + i * 0x4C + 0x18),
                    Value = Memory.ReadInt32(proc, values + i * 0x04)
                };
            }
            Remaining = Memory.ReadByte(proc, Memory.PerksRemaining);
        }

        public void Save(IntPtr proc)
        {
            IntPtr values = (IntPtr)Memory.ReadInt32(proc, Memory.PerksValuesPointer);
            for (int i = 0; i < 119; i++)
                Memory.WriteInt32(proc, values + i * 0x04, Data[i].Value);
            Memory.WriteByte(proc, Memory.PerksRemaining, Remaining);
        }
    }

    public class PerkInfo
    {
        public string Name { get; set; }
        public int Ranks { get; set; }
        public int Stat { get; set; }
        public int StatMod { get; set; }
        public int Value { get; set; }

        public string Display
        {
            get { return string.Format("{0} - {1}", Value, Name); }
        }
    }
}