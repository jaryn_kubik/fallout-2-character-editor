﻿using System;

namespace FalloutCE
{
    public class Stats
    {
        public string[] Names { get; private set; }
        public int[] Base { get; private set; }
        public int[] Bonus { get; private set; }

        private static readonly Stats instance = new Stats();
        public static Stats Instance { get { return instance; } }

        public void Reload(IntPtr proc)
        {
            Names = new string[33];
            Base = new int[33];
            Bonus = new int[33];
            for (int i = 0; i < 33; i++)
            {
                Names[i] = Memory.ReadString(proc, Memory.StatNames + i * 6 * 4);
                Base[i] = Memory.ReadInt32(proc, Memory.Player + i * 4);
                Bonus[i] = Memory.ReadInt32(proc, Memory.Player + i * 4 + 0x8C);
            }
        }

        public void Save(IntPtr proc)
        {
            for (int i = 0; i < 33; i++)
            {
                Memory.WriteInt32(proc, Memory.Player + i * 4, Base[i]);
                Memory.WriteInt32(proc, Memory.Player + i * 4 + 0x8C, Bonus[i]);
            }
        }
    }
}