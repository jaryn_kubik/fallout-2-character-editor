﻿using System;

namespace FalloutCE
{
    public class Skills
    {
        public int SkillPoints { get; set; }
        public int Karma { get; set; }
        public int Tag1 { get; set; }
        public int Tag2 { get; set; }
        public int Tag3 { get; set; }
        public int Tag4 { get; set; }
        public string[] Names { get; private set; }
        public SkillInfo[] Values { get; private set; }

        public Traits Traits { get; private set; }

        private static readonly Skills instance = new Skills();
        public static Skills Instance { get { return instance; } }

        public void Reload(IntPtr proc)
        {
            Names = new string[19];
            Values = new SkillInfo[18];
            for (int i = 0; i < 18; i++)
            {
                Names[i] = Memory.ReadString(proc, Memory.Skills + i * 0x2C);
                Values[i] = new SkillInfo
                {
                    Index = i,
                    Base = Memory.ReadInt32(proc, Memory.Skills + i * 0x2C + 0x10),
                    StatMulti = Memory.ReadInt32(proc, Memory.Skills + i * 0x2C + 0x14),
                    StatA = Memory.ReadInt32(proc, Memory.Skills + i * 0x2C + 0x18),
                    StatB = Memory.ReadInt32(proc, Memory.Skills + i * 0x2C + 0x1C),
                    Value = Memory.ReadInt32(proc, Memory.SkillValues + i * 0x04)
                };
            }
            Names[18] = "-----";

            Tag1 = Memory.ReadInt32(proc, Memory.Tag);
            Tag2 = Memory.ReadInt32(proc, Memory.Tag + 0x4);
            Tag3 = Memory.ReadInt32(proc, Memory.Tag + 0x8);
            Tag4 = Memory.ReadInt32(proc, Memory.Tag + 0xC);
            SkillPoints = Memory.ReadInt32(proc, Memory.SkillPoints);
            Karma = Memory.ReadInt32(proc, (IntPtr)Memory.ReadInt32(proc, Memory.KarmaPointer));

            Traits = new Traits(proc);
        }

        public void Save(IntPtr proc)
        {
            for (int i = 0; i < 18; i++)
                Memory.WriteInt32(proc, Memory.SkillValues + i * 0x04, Values[i].Value);

            Memory.WriteInt32(proc, Memory.Tag, getTag(Tag1));
            Memory.WriteInt32(proc, Memory.Tag + 4, getTag(Tag2));
            Memory.WriteInt32(proc, Memory.Tag + 8, getTag(Tag3));
            Memory.WriteInt32(proc, Memory.Tag + 12, getTag(Tag4));
            Memory.WriteInt32(proc, Memory.SkillPoints, SkillPoints);
            Memory.WriteInt32(proc, (IntPtr)Memory.ReadInt32(proc, Memory.KarmaPointer), Karma);

            Traits.Save(proc);
        }

        private int getTag(int tag) { return tag >= 18 ? -1 : tag; }
    }

    public class SkillInfo
    {
        public int Index { get; set; }
        public int Base { get; set; }
        public int StatMulti { get; set; }
        public int StatA { get; set; }
        public int StatB { get; set; }
        public int Value { get; set; }

        private bool IsTagged { get { return Index == Skills.Instance.Tag1 || Index == Skills.Instance.Tag2 || Index == Skills.Instance.Tag3 || Index == Skills.Instance.Tag4; } }
        public int Display
        {
            get
            {
                int display = Base + StatMulti * (getStat(StatA) + getStat(StatB));
                if (IsTagged)
                    return display + 2 * Value + 20;
                return display + Value;
            }
            set
            {
                int newValue = value - Base - StatMulti * (getStat(StatA) + getStat(StatB));
                if (IsTagged)
                    newValue = (int)Math.Ceiling((newValue - 20) / 2f);
                Value = newValue < 0 ? 0 : newValue;
            }
        }

        private int getStat(int stat)
        {
            if (stat >= 0 && stat < 7)
                return Stats.Instance.Base[stat] + Stats.Instance.Bonus[stat];
            return 0;
        }
    }
}