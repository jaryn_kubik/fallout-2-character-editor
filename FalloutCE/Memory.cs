﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FalloutCE
{
    public static class Memory
    {
        public static readonly IntPtr Player = new IntPtr(0x0051C394);
        public static readonly IntPtr SkillValues = Player + 0x0118;
        public static readonly IntPtr SkillPoints = new IntPtr(0x006681AC);
        public static readonly IntPtr Skills = new IntPtr(0x0051D118);//size 0x2C (11 * DWORD)
        public static readonly IntPtr Traits = new IntPtr(0x0051DB84);//size 0x0C (3 * DWORD)
        public static readonly IntPtr TraitValues = new IntPtr(0x0066BE40);
        public static readonly IntPtr Tag = new IntPtr(0x00668070);

        public static readonly IntPtr StatNames = new IntPtr(0x0051D53C);//6 * DWORD
        public static readonly IntPtr PerksPointer = new IntPtr(0x00496669);//19 * DWORD
        public static readonly IntPtr PerksValuesPointer = new IntPtr(0x0051C120);
        public static readonly IntPtr PerksRemaining = new IntPtr(0x00570A29);
        public static readonly IntPtr KarmaPointer = new IntPtr(0x005186C0);

        public static string ReadString(IntPtr proc, IntPtr pointer)
        {
            IntPtr address = (IntPtr)ReadInt32(proc, pointer);
            List<byte> str = new List<byte>();
            int c = 0;
            byte b;
            while ((b = ReadByte(proc, address + (c++))) != 0)
                str.Add(b);
            return Encoding.GetEncoding(1250).GetString(str.ToArray(), 0, str.Count);
        }

        public static byte ReadByte(IntPtr proc, IntPtr address)
        {
            byte[] buffer = new byte[1];
            Imports.ReadProcessMemory(proc, address, buffer, 1, IntPtr.Zero);
            return buffer[0];
        }

        public static int ReadInt32(IntPtr proc, IntPtr address)
        {
            byte[] buffer = new byte[4];
            Imports.ReadProcessMemory(proc, address, buffer, 4, IntPtr.Zero);
            return BitConverter.ToInt32(buffer, 0);
        }

        public static void WriteInt32(IntPtr proc, IntPtr address, int value)
        {
            Imports.WriteProcessMemory(proc, address, BitConverter.GetBytes(value), 4, IntPtr.Zero);
        }

        public static void WriteByte(IntPtr proc, IntPtr address, byte value)
        {
            Imports.WriteProcessMemory(proc, address, new[] {value}, 1, IntPtr.Zero);
        }
    }
}