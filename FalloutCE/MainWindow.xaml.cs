﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FalloutCE
{
    public partial class MainWindow
    {
        [STAThread]
        public static void Main() { new Application().Run(new MainWindow()); }
        public MainWindow()
        {
            InitializeComponent();
            listAll.Items.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            listMy.Items.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IntPtr proc = OpenProcess(Imports.VM_READ);

                Stats.Instance.Reload(proc);
                Skills.Instance.Reload(proc);
                Perks.Instance.Reload(proc);

                tabStats.DataContext = tabSkills.DataContext = tabPerks.DataContext = null;
                tabStats.DataContext = Stats.Instance;
                tabSkills.DataContext = Skills.Instance;
                tabPerks.DataContext = Perks.Instance;
                tabStats.IsEnabled = tabSkills.IsEnabled = tabPerks.IsEnabled = saveButton.IsEnabled = true;

                Imports.CloseHandle(proc);

                if (tabs.SelectedIndex == 0)
                    tabs.SelectedIndex = 1;
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IntPtr proc = OpenProcess(Imports.VM_READ | Imports.VM_WRITE | Imports.VM_OPERATION);

                Stats.Instance.Save(proc);
                Skills.Instance.Save(proc);
                Perks.Instance.Save(proc);

                Imports.CloseHandle(proc);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private IntPtr OpenProcess(int access)
        {
            Process[] processes = Process.GetProcessesByName("FALLOUT2");
            if (processes.Length < 1)
                throw new Exception("Process FALLOUT2 not found");
            IntPtr proc = Imports.OpenProcess(access, false, processes[0].Id);
            if (proc == IntPtr.Zero)
                throw new Exception("Unable to open FALLOUT2 process");
            return proc;
        }

        private bool justSelected;
        private void tabSkills_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!justSelected)
                return;
            tabSkills.DataContext = null;
            tabSkills.DataContext = Skills.Instance;
            justSelected = false;
        }

        private void tabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl && tabSkills.IsSelected)
                justSelected = true;
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            PerkInfo perk = listAll.SelectedItem as PerkInfo;
            if (perk == null || perk.Ranks == perk.Value)
                return;

            perk.Value++;
            if (perk.Stat != -1)
            {
                Stats.Instance.Bonus[perk.Stat] += perk.StatMod;
                tabStats.DataContext = null;
                tabStats.DataContext = Stats.Instance;
            }
            refreshSource(listMy);
        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            PerkInfo perk = listMy.SelectedItem as PerkInfo;
            if (perk == null || perk.Value == 0)
                return;

            perk.Value--;
            if (perk.Stat != -1)
            {
                Stats.Instance.Bonus[perk.Stat] -= perk.StatMod;
                tabStats.DataContext = null;
                tabStats.DataContext = Stats.Instance;
            }
            refreshSource(listMy);
        }

        private void refreshSource(FrameworkElement element)
        {
            BindingExpression binding = element.GetBindingExpression(ItemsControl.ItemsSourceProperty);
            if (binding != null)
                binding.UpdateTarget();
        }
    }
}
